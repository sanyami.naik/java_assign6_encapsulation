package Assignments.six;

import java.util.*;
class Calculate
{
    private float l;
    private float b;

    Scanner sc =new Scanner(System.in);
    public float getL() {
        System.out.println("Enter the length");
        l=sc.nextFloat();
        return l;
    }

    public void setL(float l) {
        this.l = l;
    }

    public float getB() {
        System.out.println("Enter the breadth");
        b=sc.nextFloat();
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    void area()
    {
        float answer=l*b;
        System.out.println("The area of rectangle with length " + l +" and breadth "+ b +" is "+ answer);
    }
}
public class Rectangle {
    public static void main(String[] args) {
        Calculate c = new Calculate();
        c.setL(c.getL());
        c.setB(c.getB());
        c.area();
    }
}



/*
OUTPUT:

Enter the length
43.9
Enter the breadth
34.56
The area of rectangle with length 43.9 and breadth 34.56 is 1517.1841
 */