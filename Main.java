package Assignments.six;



// file Main.java
public class Main {
    public static void main(String[] args) {
        Time currTime = new Time();  // object that stores the current time
        int hr;         // current hour obtained from currTime
        int min;        // current minute obtained from currTime
        int sec;        // current second obtained from currTime

        int[] temp;        // for using getTime()

        //currTime.setTime(20, 15, 43);
        //currTime.setTime(-55, 99, 1025);
        currTime.setTime(23, 59, 59);
        temp = currTime.getTime();
        hr = temp[0];
        min = temp[1];
        sec = temp[2];

        System.out.println(
                "The current military time is set to: "
                        + hr + ":" + min + ":" + sec
        );


        currTime.incrementTime();
        temp = currTime.getTime();
        hr = temp[0];
        min = temp[1];
        sec = temp[2];

        System.out.println(
                "After incrementing the time, the current military time is: "
                        + hr + ":" + min + ":" + sec
        );






    }
}


/*

OUTPUT:
1)Type Program 1 above into the 2 separate files specified by the header comments. Compile and run the program.
Look at the output. Does it make sense? Why or why not?
Ans:The current military time is set to: 20:15:43
    After incrementing the time, the current military time is: 20:15:44
    Yes it makes Sense.

3)Change the call to currTime.setTime() in main.java to the following:
Ans:The current military time is set to: -55:99:1025
    After incrementing the time, the current military time is: -55:99:1026
    No it does not make sense as time can never be negative

5)Add the following lines to Main.java just before the end of the main() method:
		currTime.hour = 31;
		currTime.minute = -10;
		currTime.second = 450;

		temp = currTime.getTime();
		hr = temp[0];
		min = temp[1];
		sec = temp[2];

		System.out.println(
				"After direct assignment, the current military time is: "
						+ hr + ":" + min + ":" + sec
				);
Compile and run the program.
Ans:The current military time is set to: -55:99:1025
    After incrementing the time, the current military time is: -55:99:1026
    After direct assignment, the current military time is: 31:-10:450

6)Look at the last line of output. Does it make sense? Why or why not?
7)We need to fix the problem caused by declaring the data in the Time class as public.
Change Time.java to make the 3 data declarations private. Compile the program. What happens? Why?
8)Remove the lines that were added to Main.java in step 5 above. Compile and run the program.
Ans:No it does not make sense as the minutes given are in negative
    Yes the programme runs successfully


9)Now, let’s fix the setTime() method. Change it as shown below:
	void setTime(int newHour, int newMinute, int newSecond) {
		if (newHour >= 0 && newHour <= MAX_HOURS) {
			hour = newHour;
		}
		else {
			System.out.println("Error: hour must be between 0 and 23 inclusive");
			hour = 0;
		}

		if (newMinute >= 0 && newMinute <= MAX_MIN_SECS) {
			minute = newMinute;
		}
		else {
			System.out.println("Error: minute must be between 0 and 59 inclusive");
			minute = 0;
		}

		if (newSecond >= 0 && newSecond <= MAX_MIN_SECS) {
			second = newSecond;
		}
		else {
			System.out.println("Error: second must be between 0 and 59 inclusive");
			second = 0;
		}
	}
Compile and run the program.
Ans:Error: hour must be between 0 and 23 inclusive
    Error: minute must be between 0 and 59 inclusive
    Error: second must be between 0 and 59 inclusive
    The current military time is set to: 0:0:0
    After incrementing the time, the current military time is: 0:0:1


10)Why is this version of the setTime() method more secure than the previous version?
11)Look at the new output. Does it make sense? Why or why not?
Ans:Yes this version is more secure as it is restricting the the user to put negative values for time which
    was previously not considered

12)Change the call to currTime.setTime() in Main.java to the following:
currTime.setTime(23, 59, 59);
Compile and run the program.
13)Look at the new output. Does it make sense? Why or why not?
Ans:The current military time is set to: 23:59:59
    After incrementing the time, the current military time is: 23:59:60
    Yes the output is now correct as we have also passed the correct and valid time.


14)Add an appropriate constructor to the Time class. Compile and run the program.
15)What values should be used to initialize hour, minute, and second in the constructor? Why are these times appropriate?
Ans: Time()
    {
        this.hour=00;
        this.minute=00;
        this.second=00;

    }

    Made the default values as 00 only in the constructor.

 */