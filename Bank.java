package Assignments.six;
import java.util.*;

class BankEmployee
{
    String eName;
    String eDesignation;
    private int idNo;

    public void setEName(String EName) {
        this.eName = EName;
    }

    public void setEDesignation(String EDesignation) {
        this.eDesignation = EDesignation;
    }

    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }

    @Override
    public String toString() {
        return ("The name of Bank Employee is "+ eName + ". The designation is " + eDesignation+" with ID number "+idNo);
    }
}
class Customer
{
    String cName;
    String accountType;
    private UUID accountNo;
    Scanner sc=new Scanner(System.in);

    public String getCName() {
        System.out.println("Enter the name of the Customer");
        cName=sc.nextLine();
        return cName;
    }

    public void setCName(String CName) {
        this.cName = CName;
    }

    public String getAccountType() {
        System.out.println("Enter the type of account");
        String accountType=sc.nextLine();
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setAccountNo() {
        this.accountNo = UUID.randomUUID();
    }

    public String toString() {
        return ("The name of Customer is "+ cName + ".The type of account held is  " + accountType+" with Account no  "+accountNo);
    }
}

public class Bank {
    public static void main(String[] args) {


        BankEmployee b=new BankEmployee();
        b.setEName("Vijay Shrivastav");
        b.setEDesignation("Manager");
        b.setIdNo(163);
        System.out.println(b);


        Customer c=new Customer();
        c.setCName(c.getCName());
        c.setAccountType(c.getAccountType());
        c.setAccountNo();
        System.out.println(c);



    }



}



/*OUTPUT

The name of Bank Employee is Vijay Shrivastav. The designation is Manager with ID number 163
Enter the name of the Customer
Ram Mahajan
Enter the type of account
Savings Account
The name of Customer is Ram Mahajan.The type of account held is  Savings Account with Account no  472b155a-e200-42d7-ba73-1d72d4104983

 */